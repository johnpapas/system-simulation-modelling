function dk=differEq(t,k,b,z,y)

dk(1)=b*k(1)-k(1)*z*z'*k(1);
dk(2)=k(1)*z*(-z'*k(2)+y);

end
