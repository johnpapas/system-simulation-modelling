%Author: Papas Ioannis 9218
%System Modelling and Simulation project

function [theta, error]=leastSquareMethod(iN, oN, l, input, output, t,p)

N=length(t);

%%
%Calculate the L(s) polynomial
coeffs=diag(flip(pascal(oN+1)))';
pole=l.^(0:oN);
Ls=coeffs.*pole;

%%
%Calculate the transfer functions for the phi vector
for i=1:oN
    temp=zeros(i,1)';
    temp(1)=1;
    gz(i)=tf(temp,Ls);
end

gz=flip(gz);

%%
%Calculate the numerical values at the time field for vector phi
z=zeros(iN+oN,N);

for i=1:oN
    z(i,:)=lsim(gz(i),-output,t);
end

for i=(oN+1):(iN+oN)
    z(i,:)=lsim(gz(i-iN),input,t);
end

%%
%Least square method
sumV1=zeros(iN+oN,iN+oN);
sumV2=zeros(iN+oN,1);

for iT=1:N
    sumV1=sumV1+(z(:,iT)*(z(:,iT)'));
    sumV2=sumV2+(z(:,iT)*output(iT));
end

thetaV=inv(sumV1)*sumV2;

%%
%Calculate the real theta
theta=thetaV;
theta(1:length(gz))=theta(1:length(gz))+Ls(2:length(Ls))';

%%
%Calculating the error
systemOutput=thetaV'*z;
error1=output'-systemOutput;
error=sum(error1.^2)/2;

%%
%Plot the estimated model and the real output and the error
if p==1
    figure(1)
    clf
    plot(t,systemOutput,t,output)
    title("Estimated Model")
    legend("Estimated Model", "Real Output")
    figure(2)
    clf
    plot(t,error1)
    title("Error between the estimated and the real model")
end

end