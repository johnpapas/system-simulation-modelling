function theta=onlineLSM(iN,oN,input,output,tspan,b,q0,l)

N=length(tspan);
coeffs=diag(flip(pascal(oN+1)))';
pole=l.^(0:oN);
Ls=coeffs.*pole;
k0(1,:)=inv(q0);
k0(2,:)=zeros(iN+oN,1);
k0(3,:)=zeros(iN+oN,1);

for i=1:oN
    temp=zeros(i,1)';
    temp(1)=1;
    gz(i)=tf(temp,Ls);
end

gz=flip(gz);

z=zeros(iN+oN,N);

for i=1:oN
    z(i,:)=lsim(gz(i),-output,t);
end

for i=(oN+1):(iN+oN)
    z(i,:)=lsim(gz(i-iN),input,t);
end

[t,k]=ode45(@(t,k)diferEq(t,k,b,z,output), tspan, k0);

theta=k(:,3);
theta(1:length(gz))=theta(1:length(gz))+Ls(2:length(Ls))';

% systemOutput=thetaV'*z;
% error=output'-systemOutput;
% error=mean(error);

end