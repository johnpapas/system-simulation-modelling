%Author: Papas Ioannis 9218
%System Modelling and Simulation project

t=0:0.0001:10;

u1=2*cos(t);
y1=out(t,u1);

u2=t;
y2=out(t,u2);

y3=out(t,u1+u2);

figure(1)
clf
plot(t,y1,t,u1);
title("Output for input u=2cost");
xlabel("Time")
ylabel("Output/Input")
legend("Output","Input")

figure(2)
clf
plot(t,y2,t,u2);
title("Output for input u=t");
xlabel("Time")
ylabel("Output/Input")
legend("Output","Input")

figure(3)
clf
plot(t,y3,t,u1+u2);
title("Output for input u=2cost+t");
xlabel("Time")
ylabel("Output/Input")
legend("Output","Input")

figure(4)
clf
plot(t,y1+y2,t,u1,t,u2)
title("Sum of first two outputs")
xlabel("Time")
ylabel("Output/Input")
legend("Sum of Outputs","Input u=2cost","Input u=t")

figure(5)
clf
plot(t,y3-(y1+y2))
title("Difference between 3rd output and the sum of the first and second")
xlabel("Time")
ylabel("Output")
legend("Difference")



