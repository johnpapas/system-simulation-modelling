%Author: Papas Ioannis 9218
%System Modelling and Simulation project

t=0:0.0001:10;

u=2*cos(t);
y=out(t,u);
N=length(t);
l=5;
on=8;

%Find the input and output ranks

error=zeros(on);
for oN=1:on
    for iN=1:oN
        [~,error(iN,oN)]=leastSquareMethod(iN, oN, l, u, y, t, 0);
    end
end


error(error==0)=NaN;
[minimumerror,i1]=min(abs(error));
[~,i2]=min(minimumerror);

[theta,~]=leastSquareMethod(i1(i2), i2, l, u, y, t, 0);

%Find the best pole of the polynomial to be used for linearization
l=1:1000;
poleError=zeros(length(l),1);

for iL=1:length(l)
    [~,poleError(iL)]=leastSquareMethod(i1(i2), i2, l(iL), u, y, t, 0);
end

[~,i3]=min(abs(poleError));
%Plot and calculate the mean error and the output estimated model
[finalTheta, finalError]=leastSquareMethod(i1(i2), i2, l(i3), u, y, t, 1);
    