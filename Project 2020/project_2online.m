%Author: Papas Ioannis 9218
%System Modelling and Simulation project

t=0:0.0001:10;

u=2*cos(t);
y=out(t,u);
N=length(t);
iN=3;
oN=3;
b=1;
q0=eye(iN+oN);
l=2;

theta=onlineLSM(iN,oN,u,y,t,b,q0,l);
