%System Modelling and Simulation project, Exercise 2
%Author: Papas Ioannis 
%AEM: 9218

function dy = dynamics(t,y,b,k,m)
%A function used to simulate the state variables of a system of a spring 
%and a damping. This function is used as handling function for ode45

u=5*sin(2*t)+10.5; %input

dy(1)=y(2);
dy(2)=(1/m)*u-(k/m)*y(1)-(b/m)*y(2);
dy=dy';

end

