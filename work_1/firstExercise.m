%System Modelling and Simulation project, Exercise 1
%Author: Papas Ioannis 
%AEM: 9218

%Variables
m=15;
b=0.2;
k=2;
tspan=0:0.1:10;
N=length(tspan);
yinit=[0 0];

%Input
u=5*sin(2*tspan)+10.5;

%Real Output
[t,x]=ode45(@(t,x)dynamics(t,x,b,k,m), tspan, yinit);
y=x(:,1);

%change to parameter modell y=(theta')*z
gz1=tf([-1 0],[1 2 1]);
gz2=tf(1,[1 2 1]);

%Time response of the z vector
z1=lsim(gz1,y,tspan);
z2=lsim(gz2,-y,tspan);
z3=lsim(gz2,u,tspan);

z=[z1 z2 z3]';

sumV1=zeros(3,3);
sumV2=zeros(3,1);

for iT=1:N
    sumV1=sumV1+(z(:,iT)*(z(:,iT)'));
    sumV2=sumV2+(z(:,iT)*y(iT));
end

theta=inv(sumV1)*sumV2;

%Estimating the parameters
m0=1/theta(3);
bo=(2+theta(1))*m0;
ko=(1+theta(2))*m0;

error=y-(theta'*z)';

%Plotting the error and the outputs to see the difference between the real
%and the model output
figure(1)
clf
plot(error)
xlabel("Time samples (10 sample/s)");
ylabel("Error (m)");
title("Error Plot");

figure(2)
clf
plot(y);
hold on 
plot(theta'*z);
legend("Real output", "System output")