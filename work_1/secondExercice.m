%System Modelling and Simulation project, Exercise 2
%Author: Papas Ioannis 
%AEM: 9218

%Variables
t=0:0.00001:0.5;
N=length(t);
l1=100; %The poles of the filter we'll use
vR=zeros(N,1);
vC=zeros(N,1);
bQuest=false; %If true, outliers for question b

%The output values
for iT=1:N
    Vout=v(t(iT));
    vR(iT)=Vout(2);
    vC(iT)=Vout(1);
end

if bQuest
    r=unidrnd(N,3,1);
    vR(r)=1000;
    vC(r)=3000;
end

%The input values
u1=2*sin(t);
u2=ones(N,1);

%change to parameter modell Vc=(theta')*z
gz1=tf([1 0],[1 2*l1 l1^2]);
gz2=tf(1,[1 2*l1 l1^2]);

%Time response of the z vector
z1=lsim(gz1,-vC,t);
z2=lsim(gz2,-vC,t);
z3=lsim(gz1,u2,t);
z4=lsim(gz2,u2,t);
z5=lsim(gz1,u1',t);
z6=lsim(gz2,u1',t);

z=[z1 z2 z3 z4 z5 z6]';

sumV1=zeros(6,6);
sumV2=zeros(6,1);

%The least squares parameter estimation method
for iT=1:N
    sumV1=sumV1+(z(:,iT)*(z(:,iT)'));
    sumV2=sumV2+(z(:,iT)*vC(iT));
end
theta=inv(sumV1)*sumV2;

%Calculating the real values 1/RC and 1/LC
thetaSystem=theta;
thetaSystem(1)=thetaSystem(1)+2*l1;
thetaSystem(2)=thetaSystem(2)+(l1^2);

%Mean error of the estimated modell
vcsystem=theta'*z;
vrsystem=u1+u2'-vcsystem;
errorR=vR'-vrsystem;
errorC=vC'-vcsystem;
fprintf("The mean error of the real output and the estimated output is %d", mean(errorR));

%Plotting the error and the outputs to see the difference between the real
%and the model output
figure(1)
clf
plot(errorR)
xlabel("Time samples (10^5 sample/s)")
ylabel("Error (Volt)")
title("Error of Vr plot")

figure(2)
clf
plot(vR);
hold on 
plot(vrsystem);
xlabel("Time samples (10^5 sample/s)")
ylabel("Value")
title("VR plot (model+real)")
legend("Real output", "System output")
