%System Modelling and Simulation project 2, Exercise 1
%Author: Papas Ioannis 
%AEM: 9218

function dk=diferEqAll(t,k,a,b,am,gamma)

u=5*sin(3*t);
dk(1)=-a*k(1)+b*u; %xdot
dk(2)=gamma*(k(1)-k(2)*k(4)-k(3)*k(5))*k(4); %theta1dot
dk(3)=gamma*(k(1)-k(2)*k(4)-k(3)*k(5))*k(5); %theta2dot
dk(4)=-am*k(4)+k(1); %phi1dot
dk(5)=-am*k(5)+u; %phi2dot

dk=dk';
end