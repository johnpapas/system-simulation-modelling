%System Modelling and Simulation project 2, Exercise 3
%Author: Papas Ioannis 
%AEM: 9218

function dk=diferEqBigDim(t,k,a11,a12,a21,a22,b1,b2)

u=10*sin(2*t)+5*sin(7.5*t);
dk(1)=a11*k(1)+a12*k(2)+b1*u; %x1dot
dk(2)=a21*k(1)+a22*k(2)+b2*u; %x2dot
dk(3)=k(1)*k(9)-(k(9)^2); %a11^dot
dk(4)=k(9)*k(2)-k(9)*k(10); %a21^dot
dk(5)=k(10)*k(1)-k(9)*k(10);  %a12^dot
dk(6)=k(2)*k(10)-(k(10)^2);  %a22^dot
dk(7)=(k(1)-k(9))*u;  %b1^dot
dk(8)=(k(2)-k(10))*u;  %b2^dot
dk(9)=k(3)*k(9)+k(5)*k(10)+k(7)*u;  %x1^dot
dk(10)=k(4)*k(9)+k(6)*k(10)+k(8)*u; %x2^dot

dk=dk';
end