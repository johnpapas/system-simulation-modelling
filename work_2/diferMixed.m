%System Modelling and Simulation project 2, Exercise 2
%Author: Papas Ioannis 
%AEM: 9218

function dk=diferMixed(t,k,a,b,am,h0,f)

u=5*sin(3*t);
h=h0*sin(2*pi*f*t);
k(1)=k(1)+h;
dk(1)=-a*k(1)+b*u; %x
dk(2)=-(k(1)-k(4))*k(1); %a^dot
dk(3)=(k(1)-k(4))*u; %b^dot
dk(4)=-k(2)*k(4)+k(3)*u-(am*(k(1)-k(4))); %x^dot

dk=dk';
end