%System Modelling and Simulation project 2, Exercise 1
%Author: Papas Ioannis 
%AEM: 9218

%Variables
a=2;
b=1;
gamma=30;
k0=[0 0 0 0 0];
am=4;
step=0.01;
tspan=0:step:50;
u=5*sin(3*tspan);
N=length(tspan);

%Solution of the all the differential equations
[t,k]=ode45(@(t,k)diferEqAll(t,k,a,b,am,gamma), tspan, k0);

x=k(:,1); %Real output

theta=zeros(2,N);
theta(1,:)=k(:,2); %Theta1
theta(2,:)=k(:,3); %Theta2
xestim=zeros(N,1); 
f=[k(:,4) k(:,5)]'; %�
for iT=1:N
    xestim(iT)=theta(:,iT)'*f(:,iT); %Estimated x 
end

%Plotting

figure(1)
clf
plot(tspan,theta(2,:),tspan,ones(N,1),'--')
title("Estimated b parameter")
xlabel("Time (s)")
legend("Estimated b","Real b")
figure(2)
clf
plot(tspan,am-theta(1,:),tspan,2*ones(N,1),'--')
title("Estimated a parameter")
xlabel("Time (s)")
legend("Estimated a","Real a")
figure(3)
clf
plot(tspan,x-xestim,tspan,zeros(N,1),'--')
title("Error")
xlabel("Time (s)")
figure(4)
clf
plot(tspan,x,tspan,xestim)
title("Plot of real output and estimated output")
legend("Real output", "Estimated output")