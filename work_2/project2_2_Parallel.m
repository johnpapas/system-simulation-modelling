%System Modelling and Simulation project 2, Exercise 2
%Author: Papas Ioannis 
%AEM: 9218

%Variables
a=2;
b=1;
h0=0; %Noise
freq=20; %Noise
k0=[0 0 0 0];
step=0.01;
tspan=0:step:30;
u=5*sin(3*tspan);
N=length(tspan);
if h0~=0
    noise=strcat(" with noise n0=",num2str(h0)," frequency=", num2str(freq));
else
    noise="";
end

%Differential equations solution
[t,k]=ode45(@(t,k)diferParallel(t,k,a,b,h0,freq), tspan, k0);

x=k(:,1);
a=k(:,2);
b=k(:,3);
xestim=k(:,4);

figure(1)
clf
plot(tspan,a,tspan,2*ones(N,1),'--')
title(strcat("Estimation of parameter a with Lypaunov method",noise))
xlabel("Time (s)")
legend("Estimated a","Real a")
figure(2)
clf
plot(tspan,b,tspan,ones(N,1),'--')
title(strcat("Estimated of parameter b with Lypaunov method",noise))
xlabel("Time (s)")
legend("Estimated b","Real b")
figure(3)
clf
plot(tspan,x-xestim,tspan,zeros(N,1),'--')
title("Error")
xlabel("Time (s)")


