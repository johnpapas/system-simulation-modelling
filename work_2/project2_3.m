%System Modelling and Simulation project 2, Exercise 3
%Author: Papas Ioannis 
%AEM: 9218

%Variables
a11=-0.25;
a12=3;
a21=-5;
a22=-1;
b1=1;
b2=2.2;
step=0.01;
tspan=0:step:50;
N=length(tspan);
u=10*sin(2*tspan)+5*sin(7.5*tspan);
k0=zeros(10,1);

%Differential Equation Solution
[t,k]=ode45(@(t,k)diferEqBigDim(t,k,a11,a12,a21,a22,b1,b2), tspan, k0);

x1=k(:,1);
x2=k(:,2);
a11estim=k(:,3);
a21estim=k(:,4);
a12estim=k(:,5);
a22estim=k(:,6);
b1estim=k(:,7);
b2estim=k(:,8);
x1estim=k(:,9);
x2estim=k(:,10);

%Plotting
figure(1)
clf
plot(tspan,x1,tspan,x1estim)
title("x1 and x1 estimation plot")
xlabel("Time (s)")
legend("x1", "Estimation of x1")
figure(2)
clf
plot(tspan,x2,tspan,x2estim)
title("x2 and x2 estimation plot")
xlabel("Time (s)")
legend("x2", "Estimation of x2")
figure(3)
clf
plot(x1,x2)
hold on
plot(x1estim,x2estim)
title("Phase plot of the real outputs and the estimated ones")
xlabel("Time (s)")
legend("x1-x2", "x1-x2 estimated")
figure(4)
clf
plot(tspan,a11estim,tspan,a11*ones(N,1))
title("Estimation of parameter a11")
xlabel("Time (s)")
legend("Estimation of a11", "a11")
figure(5)
clf
plot(tspan,a12estim,tspan,a12*ones(N,1))
title("Estimation of parameter a12")
xlabel("Time (s)")
legend("Estimation of a12", "a12")
figure(6)
clf
plot(tspan,a21estim,tspan,a21*ones(N,1))
title("Estimation of parameter a21")
xlabel("Time (s)")
legend("Estimation of a21", "a21")
figure(7)
clf
plot(tspan,a22estim,tspan,a22*ones(N,1))
title("Estimation of parameter a22")
xlabel("Time (s)")
legend("Estimation of a22", "a22")
figure(8)
clf
plot(tspan,b1estim,tspan,b1*ones(N,1))
title("Estimation of parameter b1")
xlabel("Time (s)")
legend("Estimation of b1", "b1")
figure(9)
clf
plot(tspan,b2estim,tspan,b2*ones(N,1))
title("Estimation of parameter b2")
xlabel("Time (s)")
legend("Estimation of b2", "b2")





